/** @type {import('tailwindcss').Config} */
module.exports = {
  presets: [require("./src/lib/tidy/theme/tidigit.tailwind.cjs")],
  purge: {
    content: ["./src/**/*.html", "./src/**/*.svelte"],
    options: {
      safelist: [
        "cs_dracula",
        "cs_dim",
        "cs_tidigit_light",
        "cs_tidigit_light_blue",
        "cs_tidigit_light_iris",
        "cs_tidigit_light_red",
        "cs_tidigit_dark",
        "cs_tidigit_dark_blue",
        "cs_tidigit_dark_red",
        "cs_tidigit_dark_bw",
        "cs_tidigit_dark_iris",
        "cs_solarized_light",
        "cs_solarized_dark",
        "border-brs1",
        "border-brs2",
        "border-brs3",
        "border-a1s1",
        "styledscroll",
        /^theme_/,
        /^cs_.*$/,
        /^bg-/,
        /^text-/
      ]
    }
  }
};
