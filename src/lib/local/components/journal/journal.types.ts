import type { HeatmapDataItem } from "$lib/tidy/components/calendarHeatmap/calendarHeatmap.types";
import type { CacheableStore } from "$lib/tidy/types/data.type";

export interface FocusHeatMapStore extends CacheableStore {
  dailyJournalDateRange: { start: Date; end: Date };
  dailyJournal: HeatmapDataItem[];
}
