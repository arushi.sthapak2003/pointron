import { get, writable } from "svelte/store";
import { PointronEventEnum } from "$lib/local/types/pointronEvent.enum";
import type { PointronEvent } from "$lib/local/types/pointronEvent.type";
import type { PointronConstants } from "$lib/local/types/pointronConstants.type";
import type { UserLocalPreferences } from "$lib/local/types/userLocalPreferences.type";
import { Persistance } from "$lib/tidy/stores/persistance";
import { Item } from "$lib/tidy/types/item.enum";
import { TimerMode } from "$lib/local/types/timerMode.enum";
import {
  SessionCompositionType,
  type SessionComposition,
  BreakCompositionType
} from "../types/sessionComposition.type";
import { generateUID } from "$lib/tidy/utils/utils";
import { toasts } from "$lib/tidy/stores/notification.store";
import { ChartType, type HorizonChart } from "$lib/tidy/types/analytics.type";
import { TimePeriodType, TimeScale } from "$lib/tidy/types/time.type";
import { objIsEmpty, shallowDiff } from "$lib/tidy/utils/obj.utils";
import { prefixTable } from "$lib/tidy/utils/text.utils";
import { Layout } from "$lib/tidy/types/layout.type";
import type { Tag, TagStore } from "../types/tag.type";
import { dataManager } from "$lib/tidy/stores/data.store";
import {
  StoreDataType,
  type CacheableStoreContract,
  PersistanceActionType
} from "$lib/tidy/types/data.type";
import { goalStore, newGoalStore, quickFocusItemStore } from "./goal.store";
import { focusItemsStore, sessionStore } from "./session.store";
import { logger } from "$lib/tidy/stores/log.store";
import { logsPaneStore, pointLogStore } from "../components/logs/log.store";
import { focusHeatmapStore } from "../components/journal/journal.store";
import { analyticsConfigStore } from "../components/analytics/analytics.store";

const appName = "Pointron";
const defaultAppMenu: string[] = ["journal", "focus", "goals", "analytics"];
const persistance = new Persistance();
export const swipeLabel = writable("");
export const defaultAppData = {
  name: appName,
  version: "0.0.1",
  leftPanelFooter: "simple",
  homePath: "focus",
  cp: {
    // modules: ["home", "goals", "insights"],
    customization: [
      "theme",
      "session",
      "analytics",
      "presets",
      "targets",
      "alerts",
      // "appMenu",
      // "widgets",
      "shortcuts"
    ],
    app: ["accessibility", "data", "feedback", "about"]
  }
};

const seedPresets: SessionComposition[] = [
  {
    id: generateUID(),
    type: SessionCompositionType.POMODORO,
    numberOfFocusRounds: 4,
    focusDuration: 28 * 60,
    breakDuration: 2 * 60,
    totalDuration: 0,
    breakReminder: 60,
    numberOfBreaks: 1,
    name: "Preset 1",
    breakType: BreakCompositionType.PREDEFINED
  },
  {
    id: generateUID(),
    type: SessionCompositionType.POMODORO,
    numberOfFocusRounds: 3,
    focusDuration: 10 * 60,
    breakDuration: 2 * 60,
    totalDuration: 0,
    breakReminder: 60,
    numberOfBreaks: 1,
    name: "Morning focus",
    breakType: BreakCompositionType.PREDEFINED
  },
  {
    id: generateUID(),
    type: SessionCompositionType.TOTAL_DURATION,
    focusDuration: 0,
    breakDuration: 10 * 60,
    totalDuration: 10 * 60 * 60,
    numberOfBreaks: 9,
    breakReminder: 60,
    name: "10 hr deep study",
    breakType: BreakCompositionType.PREDEFINED
  }
];

export const defaultHorizonChartConfiguration: HorizonChart[] = [
  {
    id: "topleft",
    period: {
      scale: TimeScale.DAYS,
      value: {
        type: TimePeriodType.RELATIVE,
        param: -1
      }
    },
    type: ChartType.PIE
  },
  {
    id: "topright",
    period: {
      scale: TimeScale.DAYS,
      value: {
        type: TimePeriodType.RELATIVE,
        param: 0
      }
    },
    type: ChartType.PIE
  },
  {
    id: "bottomleft",
    period: {
      scale: TimeScale.DAYS,
      value: {
        type: TimePeriodType.RELATIVE,
        param: -7
      }
    },
    type: ChartType.STACKEDBAR
  },
  {
    id: "bottomright",
    period: {
      scale: TimeScale.MONTHS,
      value: {
        type: TimePeriodType.RELATIVE,
        param: -6
      }
    },
    type: ChartType.STACKEDBAR
  }
];

// const userLocalPreferencesId = "Preferences:" + appName.toLowerCase();
const userLocalPreferencesId = Item.pointronPreferences;
export const seedLocalPreferences: UserLocalPreferences = {
  id: userLocalPreferencesId,
  dataType: StoreDataType.KVO,
  isEnableAgeCounter: false,
  extendDuration: 5,
  presets: seedPresets,
  isEnableAutoStartInterval: true,
  isIncludeBreakInAnalytics: false,
  timerMode: TimerMode.JOURNAL,
  appMenu: defaultAppMenu,
  manualEntryQuickDurations: [5, 10, 15, 30, 60],
  horizonCharts: defaultHorizonChartConfiguration,
  horizonsWithTarget: [],
  horizonTargets: [],
  breakReminder: 60,
  uiStates: {
    all: {
      quickFocusLayout: Layout.LIST,
      advancedComposeType: SessionCompositionType.POMODORO,
      advancedMode: 0
    },
    desktop: {
      quickFocusLayout: Layout.LIST,
      advancedComposeType: SessionCompositionType.POMODORO,
      advancedMode: 0
    },
    portrait: {
      quickFocusLayout: Layout.GRID,
      advancedComposeType: SessionCompositionType.POMODORO,
      advancedMode: 0
    }
  }
};

export const userLocalPreferences = initUserLocalPreferences();

function initUserLocalPreferences() {
  let previousValue: string;
  const {
    subscribe,
    set: setRaw,
    update
  } = writable<UserLocalPreferences>(seedLocalPreferences);
  dataManager.retrieveCache(userLocalPreferencesId).then((x) => {
    if (x) {
      setRaw(x as UserLocalPreferences);
      previousValue = JSON.stringify(x);
    }
  });
  const persist = async (n: Partial<UserLocalPreferences>) => {
    await persistance.update({
      ...n,
      id: userLocalPreferencesId,
      modifiedAt: new Date().toISOString()
    });
    cache(get(userLocalPreferences));
  };
  const cache = async (n: UserLocalPreferences) => {
    dataManager.cache(n);
  };
  const set = (x: UserLocalPreferences) => {
    setRaw(x);
    previousValue = JSON.stringify(x);
  };
  return {
    subscribe,
    update,
    loader: (data: UserLocalPreferences) => {
      data.appMenu = defaultAppMenu;
      if (!data.uiStates) data.uiStates = seedLocalPreferences.uiStates;
      if (!data.presets) data.presets = seedPresets;
      //m.horizonCharts = defaultHorizonChartConfiguration;
      if (!data.dataType) data.dataType = StoreDataType.KVO;
      set(data);
      cache(data);
    },
    loadSeedData: async () => {
      set(seedLocalPreferences);
      cache(seedLocalPreferences);
    },
    set: async (newValue: UserLocalPreferences) => {
      let changedProperties: any = {};
      if (previousValue) {
        let differences = shallowDiff(newValue, JSON.parse(previousValue));
        differences.forEach((key: string) => {
          changedProperties[key] = newValue[key as keyof UserLocalPreferences];
        });
        if (differences.some((x) => x === "horizonsWithTarget")) {
          let horizonTargets = newValue.horizonTargets?.filter((x) =>
            newValue.horizonsWithTarget?.some((y) => y === x.scale)
          );
          changedProperties.horizonTargets = horizonTargets;
        }
      }
      console.log({
        previousValue: previousValue ? JSON.parse(previousValue) : null,
        newValue,
        changedProperties
      });
      set(newValue);
      if (!objIsEmpty(changedProperties)) await persist(changedProperties);
    },
    resetHorizonChartConfiguration: () => {
      update((n: UserLocalPreferences) => {
        n.horizonCharts = defaultHorizonChartConfiguration;
        return n;
      });
      persist({ horizonCharts: defaultHorizonChartConfiguration });
    },
    updatePreset: async (preset: SessionComposition) => {
      let m = get(userLocalPreferences);
      let n = m.presets;
      let currentPresetIndex = n.findIndex((p) => p.id == preset.id);
      let presetsToRight = n.slice(currentPresetIndex + 1);
      n = n.slice(0, currentPresetIndex);
      n = [...n, preset];
      n = n.concat(presetsToRight);
      m.presets = n;
      set(m);
      return persist({ presets: n });
    },
    removePreset: async (presetId: string) => {
      let m = get(userLocalPreferences);
      let n = m.presets;
      n = n.filter((x: SessionComposition) => x.id != presetId);
      m.presets = n;
      set(m);
      return persist({ presets: n });
    },
    addPreset: async (preset: SessionComposition) => {
      let m = get(userLocalPreferences);
      m.presets.push(preset);
      set(m);
      return persist({ presets: m.presets });
    },
    updateHorizonChart: async (chart: HorizonChart) => {
      let m = get(userLocalPreferences);
      let n = m.horizonCharts;
      let currentChartIndex = n.findIndex((p) => p.id == chart.id);
      let chartsToRight = n.slice(currentChartIndex + 1);
      n = n.slice(0, currentChartIndex);
      n = [...n, chart];
      n = n.concat(chartsToRight);
      m.horizonCharts = [...n];
      set(m);
      pointronEvents.notify(PointronEventEnum.REFRESH_HORIZON_CHARTS);
      return persist({ horizonCharts: n });
    }
  };
}

export const pointronEvents = initEventStore({
  event: PointronEventEnum.NONE,
  value: false
});

function initEventStore(seed: PointronEvent) {
  const { subscribe, set, update } = writable<PointronEvent>(seed);
  return {
    subscribe,
    set: (m: PointronEvent) => {
      set(m);
    },
    reset: () => {
      update((n: PointronEvent) => {
        return { ...n, event: PointronEventEnum.NONE };
      });
    },
    notify: (m: PointronEventEnum, value: any = undefined) => {
      update((n: PointronEvent) => {
        return { ...n, value, event: m };
      });
    }
  };
}

export const pointronConstants = initPointronConstants({
  timerModes: ["Minimal", "Journal"],
  focusPlaceholderText: [
    "cooking ice cream",
    "cleaning wordle",
    "coding dishes",
    "showering",
    "draining umbrella",
    "commanding alexa"
  ],
  runningOutDuration: 5,
  gapThreshold: 60
});

function initPointronConstants(seed: PointronConstants) {
  const { subscribe, set, update } = writable<PointronConstants>(seed);
  return {
    subscribe,
    set: (m: PointronConstants) => {
      set(m);
    },
    update
  };
}

export const oasisOidcConfig = {
  authority: "https://auth.oasislabs.com",
  // Replace with your app's frontend client ID.
  client_id: import.meta.env.VITE_OASIS_CLIENT_ID,
  redirect_uri: `${window.location.origin}/play`,
  response_type: "code",
  scope: "openid profile email parcel.public",
  filterProtocolClaims: false,
  loadUserInfo: false,
  extraQueryParams: {
    audience: "https://api.oasislabs.com/parcel"
  },
  extraTokenParams: {
    audience: "https://api.oasislabs.com/parcel"
  }
};

export const seedSessions = [
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 13),
    endTime: new Date(2022, 1, 11, 14),
    id: new Date(2022, 1, 11, 13).getTime(),
    intention: ""
  },
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 14, 30),
    id: new Date(2022, 1, 11, 14, 30).getTime(),
    endTime: new Date(2022, 1, 11, 17),
    intention: "ID Card insurance project"
  },
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 17),
    id: new Date(2022, 1, 11, 17).getTime(),
    endTime: new Date(2022, 1, 11, 17, 32),
    intention: "de picto"
  },
  {
    elapsed: 60 * 60,
    focus: 47 * 60,
    brek: 13 * 60,
    extended: 0,
    startTime: new Date(2022, 1, 11, 19, 21),
    id: new Date(2022, 1, 11, 19, 21).getTime(),
    endTime: new Date(2022, 1, 11, 23),
    intention: ""
  }
];

export const seedTasks = [
  { label: "first task", estimate: 0, workedFor: 15, checked: true },
  {
    label: "second sdfasdfs task",
    estimate: 35,
    workedFor: 15,
    checked: false,
    isInprogress: true
  },
  { label: "third task", estimate: 35, workedFor: 45, checked: false }
];

const seedTagStore = {
  tags: [],
  dataType: StoreDataType.FIR,
  id: Item.PointTag,
  mutatingResources: [Item.PointTag]
};
export const tagStore = initTagStore();

function initTagStore() {
  const { subscribe, set, update } = writable<TagStore>(seedTagStore);
  dataManager.retrieveCache(Item.PointTag).then((x) => {
    if (x) {
      const n = { ...seedTagStore, tags: x.tags };
      set(n);
    }
  });
  const cache = async (n: TagStore) => {
    dataManager.cache(n);
  };
  const mutation = async (
    action: PersistanceActionType,
    record: string | Tag
  ) => {
    const data = typeof record === "string" ? { id: record } : record;
    return dataManager.performMutation(Item.PointTag, data, action);
  };
  return {
    subscribe,
    set,
    update,
    loader: async (data: Tag[]) => {
      logger.log({ context: "tagStore loader", data });
      update((x: TagStore) => {
        x.tags = data;
        cache(x);
        return x;
      });
    },
    refresh: async () => {
      logger.log("Refreshing tagStore");
      await dataManager.refresh(Item.PointTag);
    },
    create: async (label: string) => {
      const newTag = { label, id: prefixTable(generateUID(), Item.PointTag) };
      mutation(PersistanceActionType.CREATE, newTag);
      update((x: TagStore) => {
        x.tags.push(newTag);
        cache(x);
        return x;
      });
      toasts.success("Tag created successfully");
    },
    updateTag: async (tag: Tag) => {
      mutation(PersistanceActionType.UPDATE, tag);
      update((x: TagStore) => {
        x.tags = x.tags.filter((t) => t.id != tag.id);
        x.tags.push(tag);
        cache(x);
        return x;
      });
      toasts.success("Tag updated successfully");
    },
    delete: async (id: string) => {
      mutation(PersistanceActionType.DELETE, id);
      update((x: TagStore) => {
        x.tags = x.tags.filter((t) => t.id != id);
        cache(x);
        return x;
      });
      toasts.success("Tag deleted successfully");
    },
    retrieve: () => {
      subscribe((x: TagStore) => {
        return x;
      });
    }
  };
}

export const backgroundSoundStore = initBackgroundSoundStore();

function initBackgroundSoundStore() {
  const { subscribe, set, update } = writable<{
    systemSound?: string;
    youtubeUrl?: string;
  }>({});
  return {
    subscribe,
    set,
    playYoutube: (url: string) => {
      set({ youtubeUrl: url });
    },
    resetYoutube: () => {
      update((x) => {
        x.youtubeUrl = undefined;
        return x;
      });
    },
    reset: () => {
      set({ systemSound: undefined });
    }
  };
}

export const localCacheableStores: CacheableStoreContract[] = [
  goalStore,
  tagStore,
  quickFocusItemStore,
  userLocalPreferences,
  focusItemsStore,
  sessionStore,
  newGoalStore,
  pointLogStore,
  logsPaneStore,
  focusHeatmapStore,
  analyticsConfigStore
];
