import { Cloud } from "$lib/tidy/types/cloud.enum";
import { Item } from "$lib/tidy/types/item.enum";
import { get } from "svelte/store";
import { retrieveLocally } from "$lib/tidy/stores/persistance";
import { cloudProvider } from "$lib/tidy/stores/app.store";
import { SurrealDatabase } from "$lib/tidy/access/surrealHelper";
import { interceptSurrealResponse } from "$lib/tidy/utils/utils";

const surrealDb = new SurrealDatabase(import.meta.env.VITE_SURREAL_URL);
export class FocusPersistance {
  async resetSession() {
    let response;
    switch (get(cloudProvider)) {
      case Cloud.local:
        //persistLocally(Item.PointLog, logs);
        break;
      case Cloud.surreal:
        response = await surrealDb.query(`fn::pointron::focus::reset::v4()`);
        return interceptSurrealResponse(response);
    }
  }
  async retrieveLogs(date: Date, goalId: string | undefined = undefined) {
    switch (get(cloudProvider) as Cloud) {
      case Cloud.local:
        return retrieveLocally(Item.pointSessionSnapshot);
      case Cloud.surreal: {
        let response = await surrealDb.executeReadFn(
          "return fn::pointron::logs::fetch::v3($date);",
          {
            date: new Date(date).toISOString()
          }
        );
        return interceptSurrealResponse(response);
      }
      default:
        console.log("retrieveLogs default");
        return null;
    }
  }
  async fetchSession(id: string) {
    switch (get(cloudProvider) as Cloud) {
      case Cloud.local:
        return retrieveLocally(Item.pointSessionSnapshot);
      case Cloud.surreal: {
        let response = await surrealDb.executeReadFn(
          "return fn::pointron::log::fetch($id);",
          {
            id
          }
        );
        return interceptSurrealResponse(response);
      }
      default:
        return null;
    }
  }
}
