import StorageSettings from "$lib/local/components/settings/data/StorageSettings.svelte";
import SessionSettings from "$lib/local/components/settings/SessionSettings.svelte";

import Presets from "../components/focus/advanced/presets/Presets.svelte";
import WidgetSettings from "../components/settings/WidgetSettings.svelte";
import TrackingSettings from "../components/settings/targets/TrackingSettings.svelte";
import Focus from "../components/focus/Focus.svelte";
import Zen from "../components/focus/zen/Zen.svelte";
import FocusPlayer from "../components/focus/player/FocusPlayer.svelte";
import { ActionType, type Action } from "$lib/tidy/types/action.type";
import Journal from "../components/journal/Journal.svelte";
import { PointronEventEnum } from "../types/pointronEvent.enum";
import AddTagModal from "../components/tag/AddTagModal.svelte";
import ImportAppData from "../components/settings/ImportAppData/ImportAppData.svelte";
import TagsList from "../components/settings/tags/TagsList.svelte";
import ConvertToSubGoalModal from "../components/goals/ConvertToSubGoalModal.svelte";
import EditPresetView from "../components/focus/advanced/presets/EditPresetModal.svelte";
import Onboarding from "../components/onboarding/Onboarding.svelte";
import ComposeByEndTimeModal from "../components/focus/advanced/composition/ComposeByEndTimeModal.svelte";
import ComposeModal from "../components/focus/advanced/composition/ComposeModal.svelte";
import ComingSoonView from "$lib/tidy/elements/ComingSoonView.svelte";
import PresetSaveConfirmationModal from "../components/focus/advanced/presets/PresetSaveConfirmationModal.svelte";
import SessionFinishedModal from "../components/focus/elements/SessionFinishedModal.svelte";
import EditTagModal from "../components/tag/EditTagModal.svelte";
import AnalyticsSettings from "../components/settings/AnalyticsSettings.svelte";
import Think from "../components/focus/Think.svelte";
import BackgroundMusic from "../components/focus/backgroundMusic/BackgroundMusic.svelte";
import { sessionStore } from "./session.store";
import { Size } from "$lib/tidy/types/size.enum";
import { ButtonVariant } from "$lib/tidy/types/button.type";
import { get } from "svelte/store";
import { Item } from "$lib/tidy/types/item.enum";
import { Persistance } from "$lib/tidy/stores/persistance";
import modalEvent from "$lib/tidy/components/modal/modal.store";
import {
  toasts,
  confirmationNotification
} from "$lib/tidy/stores/notification.store";
import { AlertType } from "$lib/tidy/types/notification.type";
import { generateUID } from "$lib/tidy/utils/utils";
import CreateGoal from "../components/goals/create/CreateGoal.svelte";
import { pointronEvents } from "./local.store";
import GoalHomeV2 from "../components/goals/home/GoalHomeV2.svelte";
import FocusItemsModal from "../components/focus/advanced/FocusItemsModal.svelte";
import BreakReminderModal from "../components/focus/elements/BreakReminderModal.svelte";
import PredefinedIntervalNotifierOverlay from "../components/focus/elements/PredefinedIntervalNotifierOverlay.svelte";
import { pointLogStore } from "../components/logs/log.store";
import ControlPanelLogsPane from "../components/logs/ControlPanelLogsPane.svelte";
import SessionLogPage from "../components/logs/logPage/SessionLogPage.svelte";
import ManualLogPane from "../components/logs/manualLog/ManualLogPane.svelte";
import LogsPane from "../components/logs/LogsPane.svelte";
import AnalyticsV2 from "../components/analytics/AnalyticsV2.svelte";

const isSessionRunningPreCondition = () => get(sessionStore).isSessionRunning;

export const localActions: Action[] = [
  {
    action: PointronEventEnum.FULL_SCREEN_FOCUS,
    component: Zen,
    icon: "zen",
    type: ActionType.META_MODAL,
    associatedPlayer: PointronEventEnum.FOCUS_PLAYER,
    modalParams: {
      layout: {
        ignoreSafeArea: true,
        size: Size.full
      }
    }
  },
  {
    action: PointronEventEnum.SESSION_FINISHED,
    component: SessionFinishedModal,
    type: ActionType.META_MODAL,
    modalParams: {
      isDismissable: false,
      layout: {
        primaryAction: {
          label: "Done",
          callback: () => sessionStore.close()
        }
      }
    }
  },
  {
    action: PointronEventEnum.SAVE_PRESET_MODAL,
    component: PresetSaveConfirmationModal,
    type: ActionType.META_MODAL,
    modalParams: {
      layout: {
        size: Size.xs,
        primaryAction: {
          label: "Save Preset",
          callback: () => sessionStore.saveCurrentCompositionAsPreset()
        },
        secondaryAction: {
          label: "Cancel"
        }
      }
    }
  },
  {
    action: PointronEventEnum.SESSION_LOG_MODAL,
    component: SessionLogPage,
    type: ActionType.META_MODAL,
    modalParams: {
      title: "Session Log",
      layout: {
        size: Size.xl,
        isShowClose: true
      }
    }
  },
  {
    action: PointronEventEnum.COMPOSE_BY_END_TIME_MODAL,
    component: ComposeByEndTimeModal,
    type: ActionType.META_MODAL,
    modalParams: {
      title: "Choose end time",
      layout: {
        secondaryAction: {
          label: "Done"
        },
        size: Size.lg
      }
    }
  },
  {
    action: PointronEventEnum.COMPOSE_TIME_MODAL,
    component: ComposeModal,
    type: ActionType.META_MODAL,
    modalParams: {
      layout: {
        size: Size.lg,
        primaryAction: {
          label: "Proceed"
        },
        secondaryAction: {
          label: "Cancel"
        }
      }
    }
  },
  {
    action: PointronEventEnum.MANUAL_FOCUS_ENTRY_POP,
    component: ManualLogPane,
    get label() {
      return this.modalParams?.title;
    },
    type: ActionType.MODAL,
    modalParams: {
      title: "Manual time entry",
      isShowAsSheet: true,
      layout: {
        size: Size.xl,
        primaryAction: {
          label: "Save entries",
          callback: () => pointLogStore.saveManualLogs()
        },
        secondaryAction: {
          label: "Discard",
          callback: () => {
            setTimeout(() => {
              pointLogStore.reset();
            }, 100);
            return Promise.resolve(true);
          }
        }
      }
    }
  },
  {
    action: PointronEventEnum.EDIT_PRESET,
    component: EditPresetView,
    type: ActionType.MODAL,
    label: "Create a new preset",
    modalParams: {
      title: "Edit Preset",
      layout: {
        size: Size.xl
      }
    }
  },
  {
    action: PointronEventEnum.BREAK_REMINDER,
    component: BreakReminderModal,
    type: ActionType.META_MODAL,
    modalParams: {
      layout: {
        size: Size.lg,
        secondaryAction: {
          label: "Continue working"
        },
        primaryAction: {
          label: "Take break",
          callback: () => sessionStore.startBreak()
        }
      }
    }
  },
  {
    action: PointronEventEnum.PREDEFINED_INTERVAL_NOTIFIER,
    component: PredefinedIntervalNotifierOverlay,
    type: ActionType.META_MODAL,
    modalParams: {
      isShowOverlay: false,
      layout: {
        size: Size.xs,
        ignoreSafeArea: true
      }
    }
  },
  {
    action: PointronEventEnum.ADD_TAG,
    component: AddTagModal,
    type: ActionType.MODAL,
    label: "Add Tag",
    modalParams: {
      title: "Add Tag",
      layout: {
        size: Size.xs
      }
    }
  },
  {
    action: PointronEventEnum.EDIT_TAG,
    type: ActionType.META_MODAL,
    component: EditTagModal,
    modalParams: {
      layout: {
        size: Size.xs
      }
    }
  },
  {
    action: PointronEventEnum.CONVERT_TO_SUBGOAL,
    component: ConvertToSubGoalModal,
    type: ActionType.META_MODAL
  },
  {
    action: PointronEventEnum.IMPORT_APP_DATA,
    component: ImportAppData
  },
  {
    action: PointronEventEnum.LOGS,
    component: LogsPane,
    isMenuHidden: true,
    label: "Logs",
    cmdLabel: "See Logs",
    type: ActionType.MODAL,
    icon: "history",
    modalParams: {
      isShowAsSheet: true,
      layout: {
        size: Size.xl
      }
    }
  },
  {
    action: "onboarding",
    component: Onboarding,
    isMenuHidden: true,
    label: "Onboarding",
    type: ActionType.META_PAGE
  },
  {
    action: "cplogs",
    component: ControlPanelLogsPane,
    path: "cp/logs",
    label: "Logs",
    type: ActionType.META_PAGE,
    icon: "history"
  },
  {
    action: "cptags",
    component: TagsList,
    path: "cp/tags",
    label: "Tags",
    icon: "tag",
    type: ActionType.PAGE
  },
  {
    action: "journal",
    component: Journal,
    icon: "journal",
    label: "Journal",
    type: ActionType.PAGE
  },
  {
    action: PointronEventEnum.FOCUS_PLAYER,
    type: ActionType.META,
    component: FocusPlayer
  },
  {
    action: "focus",
    component: Focus,
    icon: "focus",
    type: ActionType.PAGE,
    label: "Focus"
  },
  {
    action: "analytics",
    component: AnalyticsV2,
    type: ActionType.PAGE,
    icon: "chart",
    label: "Analytics"
  },
  {
    action: "goals",
    component: GoalHomeV2,
    icon: "goals",
    type: ActionType.PAGE,
    label: "Goals"
  },
  {
    action: "session-settings",
    cmdLabel: "Session Settings",
    label: "Session",
    path: "cp/session",
    icon: "clock",
    type: ActionType.PAGE,
    component: SessionSettings
  },
  {
    action: PointronEventEnum.SESSION_SETTINGS_MODAL,
    label: "Session Settings",
    type: ActionType.MODAL,
    component: SessionSettings,
    modalParams: {
      layout: {
        size: Size.md,
        primaryAction: {
          label: "Done"
        }
      }
    }
  },
  {
    action: "alerts",
    cmdLabel: "Alert Settings",
    label: "Alerts",
    path: "cp/alerts",
    icon: "bell",
    type: ActionType.PAGE,
    component: ComingSoonView
  },
  {
    action: "presets",
    label: "Presets",
    path: "cp/presets",
    icon: "folder",
    type: ActionType.PAGE,
    component: Presets
  },

  {
    action: "importexport",
    label: "Import / Export data",
    path: "cp/importexport",
    icon: "data",
    type: ActionType.PAGE,
    isInactive: true,
    component: StorageSettings
  },
  {
    action: "widgets",
    label: "Widgets",
    path: "cp/widgets",
    icon: "widget",
    type: ActionType.PAGE,
    isInactive: true,
    component: WidgetSettings
  },
  {
    action: "targets",
    cmdLabel: "Target Settings",
    label: "Targets",
    path: "cp/targets",
    icon: "fire",
    type: ActionType.PAGE,
    component: TrackingSettings
  },
  {
    action: "analytics-settings",
    cmdLabel: "Analytics Settings",
    label: "Analytics",
    path: "cp/analytic-settings",
    icon: "chart",
    type: ActionType.PAGE,
    component: AnalyticsSettings
  },
  {
    action: "test",
    label: "Test",
    path: "test",
    type: ActionType.META_PAGE
  },
  {
    action: PointronEventEnum.BACKGROUND_MUSIC,
    label: "Background music",
    type: ActionType.MODAL,
    cmdBarPreCondition: isSessionRunningPreCondition,
    component: BackgroundMusic,
    modalParams: {
      layout: {
        size: Size.lg,
        primaryAction: {
          label: "Done"
        }
      }
    }
  },
  {
    action: "finishSession",
    label: "Finish the session",
    fn: sessionStore.finishSession,
    cmdBarPreCondition: isSessionRunningPreCondition,
    type: ActionType.FUNCTION
  },
  {
    action: "pinAGoal",
    label: "Pin a goal to quick focus",
    type: ActionType.SEARCH_CMD,
    searchActionParams: {
      searchItemType: Item.PointGoal,
      itemLabel: "goal",
      callback: (id: string, label?: string) => {
        console.log("search action selected id:", { id });
        new Persistance().update({ isPinnedForQuickStart: true, id });
        toasts.trigger({
          title: "Goal: " + label,
          message: "Pinned to quick focus",
          actionText: "View",
          type: AlertType.SUCCESS,
          id: generateUID(),
          callback: () => {}
        });
      }
    }
  },
  {
    action: PointronEventEnum.ABANDON_SESSION,
    label: "Abandon the current session",
    fn: sessionStore.close,
    type: ActionType.CONFIRMATION,
    cmdBarPreCondition: isSessionRunningPreCondition,
    confirmation: {
      title: "Abandon focus session",
      message: "Are you sure you want to abandon this focus session?",
      confirmAction: {
        label: "Abandon",
        variant: ButtonVariant.DANGER,
        callback: () => {
          return Promise.resolve(sessionStore.close());
        }
      }
    }
  },
  {
    action: PointronEventEnum.CREATE_EDIT_GOAL,
    get label() {
      return this.modalParams?.title;
    },
    type: ActionType.MODAL,
    component: CreateGoal,
    modalParams: {
      title: "Create a new goal",
      isShowAsSheet: true,
      layout: {
        size: Size.lg
      }
    }
  },
  {
    action: PointronEventEnum.THINK_MODE,
    label: "Think mode",
    icon: "think",
    type: ActionType.MODAL,
    cmdBarPreCondition: isSessionRunningPreCondition,
    component: Think,
    modalParams: {
      layout: {
        size: Size.full,
        ignoreSafeArea: true
      }
    }
  },
  {
    action: PointronEventEnum.DELETE_SESSION,
    type: ActionType.FUNCTION,
    fn: async (params: any) => {
      confirmationNotification.notify({
        title: "Delete session",
        message: "Are you sure you want to delete this session log?",
        confirmAction: {
          label: "Delete",
          icon: "trash",
          variant: ButtonVariant.DANGER,
          callback: async () => {
            const response = await pointLogStore.deleteLog(params.id);
            if (response) {
              toasts.success("Session log deleted successfully");
              pointronEvents.notify(PointronEventEnum.REFRESH_LOGS);
            } else {
              toasts.error("Failed to delete session log");
            }
            modalEvent.hideSpecific(PointronEventEnum.SESSION_LOG_MODAL);
          }
        }
      });
    }
  },
  {
    action: PointronEventEnum.SHOW_FOCUSITEMS_MODAL,
    type: ActionType.MODAL,
    component: FocusItemsModal,
    modalParams: {
      title: "Focus Items",
      isShowAsSheet: false,
      layout: {
        size: Size.lg,
        secondaryAction: {
          label: "Done"
        }
      }
    }
  }
];
