import { AppDexie } from "$lib/tidy/stores/dexie";

export class LocalDexie extends AppDexie {
  constructor(scope: string) {
    super(scope);
  }
}
