export enum TaskStatus {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED
}