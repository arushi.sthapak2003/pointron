export enum HeaderType {
  STICKY = "STICKY",
  FIXED = "FIXED",
  NORMAL = "NORMAL",
}
