export type TimeStopOffsetRange = {
  from: number;
  to: number;
};
