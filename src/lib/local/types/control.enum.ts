export enum Control {
  START,
  BREAK,
  FINISH,
  RESUME,
  SKIPBREAK,
  EXTEND,
  ABANDON,
}
