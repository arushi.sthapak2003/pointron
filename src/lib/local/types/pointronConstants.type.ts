export type PointronConstants = {
  timerModes: string[];
  focusPlaceholderText: string[];
  runningOutDuration: number;
  gapThreshold: number;
};
